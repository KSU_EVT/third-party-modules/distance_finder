#include "distance_finder.hpp"
int main(int argc, char **argv){

    ros::init(argc, argv, "test");
    ros::NodeHandle nh;
    distance_finder::DistanceFinder distance_finder(nh);
    ros::spin();
    return 0;
}