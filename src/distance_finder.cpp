#include "distance_finder.hpp"

namespace distance_finder{
    DistanceFinder::DistanceFinder(ros::NodeHandle& nh):nh_(nh)
    {
        loadParameters();
        subscribeToTopics();
        publishToTopics();
   }

   void DistanceFinder::subscribeToTopics()
   {
       laser_subscriber_=nh_.subscribe(laser_scan_topic_name_,1,&distance_finder::DistanceFinder::laserCallback,this);
   }

   void DistanceFinder::publishToTopics()
   {
       ackermann_command_=nh_.advertise<ackermann_msgs::AckermannDrive>(ackermann_command_topic_name_,1);

   }
   void DistanceFinder::loadParameters(){
        ROS_INFO("loading handle parameters");
        if (!nh_.param<std::string>("ackermann_command_topic_name", ackermann_command_topic_name_, "/car_1/command")) {
        ROS_WARN_STREAM(
            "Did not load ackermann_command_topic_name. Standard value is: " << ackermann_command_topic_name_);
        }
        if (!nh_.param<std::string>("laser_scan_topic_name", laser_scan_topic_name_, "/car_1/scan")) {
        ROS_WARN_STREAM(
            "Did not load laser_scan_topic_name. Standard value is: " << laser_scan_topic_name_);
        }
        if (!nh_.param<float>("theta_angle", theta_, 1.22173)) {
        ROS_WARN_STREAM(
            "Did not load theta_angle. Standard value is: " << theta_);
        }
        if (!nh_.param<int>("upper_index", upper_index_, 460)) {
        ROS_WARN_STREAM(
            "Did not load upper_index. Standard value is: " << upper_index_);
        }
        if (!nh_.param<int>("right_index", right_index_, 180)) {
        ROS_WARN_STREAM(
            "Did not load right_index. Standard value is: " << right_index_);
        }
        if (!nh_.param<float>("ACerror", ACerror_, .5)) {
        ROS_WARN_STREAM(
            "Did not load ACerror. Standard value is: " << ACerror_);
        }        
        if (!nh_.param<float>("desired_dist", desired_dist_, 1)) {
        ROS_WARN_STREAM(
            "Did not load desired_dist. Standard value is: " << desired_dist_);
        }        
        if (!nh_.param<float>("kp", kp_, .1)) {
        ROS_WARN_STREAM(
            "Did not load kp. Standard value is: " << kp_);
        }        
        if (!nh_.param<float>("kd", kd_, .1)) {
        ROS_WARN_STREAM(
            "Did not load kd. Standard value is: " << kd_);
        }        
        if (!nh_.param<float>("set_vel", set_vel_, .25)) {
        ROS_WARN_STREAM(
            "Did not load set_vel. Standard value is: " << set_vel_);
        }   
        if (!nh_.param<float>("abs_servo_max", abs_servo_max_, 2)) {
        ROS_WARN_STREAM(
            "Did not load abs_servo_max. Standard value is: " << abs_servo_max_);
        }        
        if (!nh_.param<float>("servo_speed", servo_speed_, 1)) {
        ROS_WARN_STREAM(
            "Did not load servo_speed_. Standard value is: " << servo_speed_);
        } 
   }
   void DistanceFinder::laserCallback(const sensor_msgs::LaserScan scan){
       calcAlpha(scan);
       calcCD();
       command_.speed=set_vel_;
       command_.steering_angle_velocity=servo_speed_;
       PIDcontroller();
       command_.steering_angle=servo_pos_;
       prev_CDdist_=CDdist_;
       ackermann_command_.publish(command_);
    }
    void DistanceFinder::calcAlpha(const sensor_msgs::LaserScan scan){
       alpha_=atan(((scan.ranges[upper_index_]*cos(theta_))-scan.ranges[right_index_])/(scan.ranges[upper_index_]*sin(theta_)));
       ABdist_=scan.ranges[right_index_]*cos(alpha_);
    }
    void DistanceFinder::calcCD(){
        CDdist_=ABdist_+(ACerror_*sin(alpha_));
    }
    /*
    # Assumes Ackermann front-wheel steering. The left and right front
    # wheels are generally at different angles. To simplify, the commanded
    # angle corresponds to the yaw of a virtual wheel located at the
    # center of the front axle, like on a tricycle.  Positive yaw is to
    # the left. (This is *not* the angle of the steering wheel inside the
    # passenger compartment.)
    */
    //our PID is controlling the servo which for the case of the simulator is in radians
    //our PID needs to go from values of negative abs_servo_max (full right) to positive abs_servo_max (full left)
    void DistanceFinder::PIDcontroller(){
        servo_pos_=((kp_*(desired_dist_-CDdist_))+(kd_*(prev_CDdist_-CDdist_)));
    }
    
   
}